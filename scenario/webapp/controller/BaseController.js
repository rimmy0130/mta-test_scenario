sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History",
	"sap/ui/core/UIComponent",
	"sap/m/MessageBox",
	"sap/ui/model/json/JSONModel"
], function(Controller, History, UIComponent, MessageBox, JSONModel) {
	"use strict";

	return Controller.extend("com.istn.sol.ci.scenario.controller.BaseController", {
		getRouter : function () {
			return UIComponent.getRouterFor(this);
		},

		onNavBack: function () { 
			var oHistory, sPreviousHash;

			oHistory = History.getInstance();
			sPreviousHash = oHistory.getPreviousHash();
			
			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				this.getRouter().navTo("Scenario", {}, true /*no history*/);
			}
		},
		
		
		
	});
});