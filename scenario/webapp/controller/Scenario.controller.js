sap.ui.define([
	"./BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageBox",
	"../model/formatter",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
], function (BaseController, JSONModel, MessageBox, formatter, Filter, FilterOperator) {
	"use strict";

	return BaseController.extend("com.istn.sol.ci.scenario.controller.Scenario", {
		formatter: formatter,
		
		onInit: function () {
			this._getScenarios();
			this._initialization();
			this.getOwnerComponent().reload = true;
			this.getRouter().getRoute("Scenario").attachPatternMatched(this._onScenarioMatched, this);
		},
		
		_onScenarioMatched : function(){
			if(this.getOwnerComponent().reload){
				this._getScenarios();
				this.getOwnerComponent().reload = false;
			}
			
		},
		
		onAfterRendering: function() {
			// this.getView().addStyleClass(this.getOwnerComponent().getContentDensityClass());
			this._addEventDelegate();
			// this._getScenarios();
		},
		
		/**
		 * 공통
		 */
		_initialization : function(){
			this.i18n = this.getOwnerComponent().getModel("i18n").getResourceBundle();
			this.getOwnerComponent().setModel(new JSONModel(), "scenarios");
			this.getView().setModel(new JSONModel({
				Scenario: ""  //ScenarioCode
			}), "filterBar"); 
			
			this.getView().setModel(new JSONModel({
				scenarioPage : {
					busy : false
				},
				scenarioTable : {
					visibleCount : 0,
					noDataText : this.i18n.getText("scenario.main.common.scenarioNoDataText")
				}
			}), "layout");
		},
		
		
		/**
		* 화면 관련
		*/
		//데이터 호출
		_getScenarios : function(){
			this.getView().getModel("layout").setProperty("/scenarioPage/busy", true);
			
			var that = this;
			var qs = "?$expand=_fields&$orderby=modifiedAt";
			$.ajax({
				url : "/odata_scenario/Admin/Scenarios" + qs,
				method : "GET",
				success : function(data){ 
					that.getOwnerComponent().getModel("scenarios").setData(data.value);
					that.getView().getModel("layout").setProperty("/scenarioTable/visibleCount", data.value.length);
				},
				error : function(error){
					MessageBox.error("데이터를 못가져 옵니다.");
				},
				complete : function(){
					that.getView().getModel("layout").setProperty("/scenarioPage/busy", false);
				}
			});
		},
		
		//필터바 검색 조건 입력 시 테이블 블락
		onFilterBarScenarioChange : function(event){
			this.byId("id-table-scenario").setBlocked(true);
		},
		
		//필터바 검색
		onScenarioFilterBarSearch : function(event){
			var filters = [] ;
			var filterBar = this.getView().getModel("filterBar").getData();
			var filterBarKeyArr = Object.keys(filterBar);
			
			filterBarKeyArr.forEach(function(key){
				filters.push(new Filter(key, FilterOperator.Contains, filterBar[key].replace(/(^\s*)|(\s*$)/g, "")));
			});
			
			this.byId("id-table-scenario").getBinding("rows").filter(filters);
			// var aa = this.byId("id-table-scenario").getBinding("rows").filter(filters);
			this.byId("id-table-scenario").setBlocked(false);
			
			this._setVisibleRowCount();
		},
		
		
		
		/**
		 * 테이블_table 관련
	 	 */
		//시나리오 생성화면 이동
		onScenarioCreate : function(){
			this.getRouter().navTo("ScenarioCreate");
			// this.getRouter().navTo("ScenarioDisplay",{
			// 	rowIdx: 0
			// });  //test ing...
		},
		
		//시나리오 상세화면 이동
		onCellClickScenarioRow : function(event){
			var rowIdx = event.getParameter("rowBindingContext").getPath().split("/").reverse()[0];
			this.getRouter().navTo("ScenarioDisplay",{
				rowIdx: rowIdx
			});
		},
		
		_addEventDelegate : function(){
			var that = this;
			var scenarioTable = this.getView().byId("id-table-scenario");
			scenarioTable.addEventDelegate({
				onAfterRendering: function(){
					that._setVisibleRowCount();
				}
			});
			
		},
		
		//테이블 visible Row Count 셋팅
		_setVisibleRowCount : function(){
			var scenarioTable = this.getView().byId("id-table-scenario");
			var scenarioBindingLength = this.byId("id-table-scenario").getBinding("rows").getLength();
			
			var top = $('#' + scenarioTable.getId()).offset().top;
			var height = $(document).height();
			// var rowHight = $(scenarioTable.getAggregation("rows")[0].getDomRef()).height();
			var rowHight = 40;
			var rowCount = Math.trunc((height-top)/rowHight);

			if(rowCount > scenarioBindingLength){
				rowCount =  scenarioBindingLength;
			}
			
			scenarioTable.setVisibleRowCount(rowCount);
		}


	});
});