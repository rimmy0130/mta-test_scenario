sap.ui.define([
], function() {
	"use strict";
	
	var Formatter = {
		_getFieldCount : function(fieldArr){
			if(fieldArr){
				return fieldArr.length;
			}else{
				return "";
			}
		}
	};
	return Formatter;
});